<?php
// For Cpanel (PHP) hostings
// This script will download a zip from
// an url and extract to to a given directory.
// The idea is to update a site just calling
// the update.php url from a webhook.
// Easier than uploading via FTP xd.
// Copy this file and modify as you please.


// This function copy $source directory and all files
// and sub directories to $destination folder
// https://gist.github.com/gserrano/4c9648ec9eb293b9377b

function recursive_copy($src, $dst) {
	$dir = opendir($src);
	@mkdir($dst);
	while(( $file = readdir($dir)) ) {
		if (( $file != '.' ) && ( $file != '..' )) {
			if ( is_dir($src . '/' . $file) ) {
				recursive_copy($src .'/'. $file, $dst .'/'. $file);
			}
			else {
				copy($src .'/'. $file,$dst .'/'. $file);
			}
		}
	}
	closedir($dir);
}

// https://stackoverflow.com/a/3352564
function removedir($dir = "./tmp") {
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::CHILD_FIRST
    );

    foreach ($files as $fileinfo) {
        $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
        $todo($fileinfo->getRealPath());
    }

    rmdir($dir);
}

// Download latest version from the sources
// For a more advanced downloader maybe check
// https://github.com/jakiestfu/Repo-Downloader/tree/master
function download($url) {
    $filename = basename($url);
    $downloaded = file_put_contents($filename, file_get_contents($url));

    if (!$downloaded) {
        die("Error downloading from " . $url);
    }

    return $filename;
}

function unarchive($filename, $to = "./tmp") {
  $zip = new ZipArchive;
  $zip->open($filename);

  $zip->extractTo($to);
  $zip->close();

  unlink($filename);
}

// If the website was taking too long to download
// we can use a git repo strategy.
// Init the git repo directory somewhere in the server
// and we can run git commands there.
function download_from_git($repo, $origin = "origin", $branch = "main") {
  $status = shell_exec("cd $repo && git pull $origin $branch && git show --oneline --name-status");
  return $status;
}

// Downloads and copies the zip content inside the root directory
// then removes all unwanted files
function use_zip() {
    // Replace with the correct url
    $url = "https://website/main.zip";
    $temp = "./tmp";

    unarchive(download($url), $temp);

    recursive_copy("$temp/website/docs/", "./");
    removedir($temp);
    echo "OK";
}

// Just use the git commands to get the latest version
// and copies the results
function use_git() {
  $repo = "/user/myuser/git_repo";
  $status = download_from_git($repo);
  recursive_copy("$repo/docs/", "./");
  echo $status;
}

function main() {
  // Select which strategy to use to update the website
  // use_zip();
  use_git();
}

main();

?>
