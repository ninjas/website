.PHONY: build html server format reload new publish compress

build b:
	@rm -rf docs/
	@mkdir docs/
	@make html

html h:
	@cp -R sources/assets/* docs/
	sources/build.exs

# An infinite loop to rebuild each second
reload r:
	while sleep 1; do make html; done

server s:
	@make build
	@echo "http://localhost:8000"
	@cd docs && python3 -m http.server

format f:
	mix format sources/build.exs
	mix format sources/new.exs
	mix format sources/compress.exs
	mix format sources/templates/home.html.eex
	mix format sources/templates/post.html.eex
	mix format sources/templates/page.html.eex

new n:
	sources/new.exs "$(title)"

publish p:
	make build
	make compress
	git add .
	git commit -m 'updated site'
	git push origin main
	./update.sh

# Use Imagemagick
# https://gist.github.com/JesseRWeigel/756cc507cbe59084d66e376eaf05c6e9
compress c:
	@echo "Optimizing images"
	sources/compress.exs
