#!sh
# Update Website to
# the latest version
echo "Updating https://my-website-url"
# Call the update.php from the website to update it in the hosting
# Or use a custom name with a random number for more security
curl https://my-website-url/update.php
