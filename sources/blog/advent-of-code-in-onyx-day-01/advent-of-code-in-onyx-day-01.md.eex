---
{
  "author": "Camilo Castro <camilo@ninjas.cl>",
  "author_url": "https://ninjas.cl",
  "description": "A solution of Day 01 from Advent of Code 2024 in Onyx",
  "image": "https://ninjas.cl/blog/advent-of-code-in-onyx-day-01/assets/cover.jpg",
  "published": true,
  "published_at": "2024-12-04T15:14:22.033251Z",
  "slug": "advent-of-code-in-onyx-day-01",
  "tags": ["avent of code", "2024", "str", "conv", "iterator", "slice", "onyx", "elixir"],
  "title": "Advent of Code - Day 01 Solution"
}
---

![https://commons.wikimedia.org/wiki/File:Onyx_Mainzerbecken.jpg](assets/cover.jpg)

- See also: [https://adventofcode.com/2024/](https://adventofcode.com/2024/)

## --- Day 1: Historian Hysteria ---

The Chief Historian is always present for the big Christmas sleigh launch, but nobody has seen him in months! Last anyone heard, he was visiting locations that are historically significant to the North Pole; a group of Senior Historians has asked you to accompany them as they check the places they think he was most likely to visit.

As each location is checked, they will mark it on their list with a star. They figure the Chief Historian must be in one of the first fifty places they'll look, so in order to save Christmas, you need to help them get fifty stars on their list before Santa takes off on December 25th.

Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!

You haven't even left yet and the group of Elvish Senior Historians has already hit a problem: their list of locations to check is currently empty. Eventually, someone decides that the best place to check first would be the Chief Historian's office.

Upon pouring into the office, everyone confirms that the Chief Historian is indeed nowhere to be found. Instead, the Elves discover an assortment of notes and lists of historically significant locations! This seems to be the planning the Chief Historian was doing before he left. Perhaps these notes can be used to determine which locations to search?

Throughout the Chief's office, the historically significant locations are listed not by name but by a unique number called the location ID. To make sure they don't miss anything, The Historians split into two groups, each searching the office and trying to create their own complete list of location IDs.

There's just one problem: by holding the two lists up side by side (your puzzle input), it quickly becomes clear that the lists aren't very similar. Maybe you can help The Historians reconcile their lists?

For example:

```text
3   4
4   3
2   5
1   3
3   9
3   3
```

Maybe the lists are only off by a small amount! To find out, pair up the numbers and measure how far apart they are. Pair up the smallest number in the left list with the smallest number in the right list, then the second-smallest left number with the second-smallest right number, and so on.

Within each pair, figure out how far apart the two numbers are; you'll need to add up all of those distances. For example, if you pair up a 3 from the left list with a 7 from the right list, the distance apart is 4; if you pair up a 9 with a 3, the distance apart is 6.

In the example list above, the pairs and distances would be as follows:

- The smallest number in the left list is 1, and the smallest number in the right list is 3. The distance between them is 2.
- The second-smallest number in the left list is 2, and the second-smallest number in the right list is another 3. The distance between them is 1.
- The third-smallest number in both lists is 3, so the distance between them is 0.
- The next numbers to pair up are 3 and 4, a distance of 1.
- The fifth-smallest numbers in each list are 3 and 5, a distance of 2.
- Finally, the largest number in the left list is 4, while the largest number in the right list is 9; these are a distance 5 apart.

To find the total distance between the left list and the right list, add up the distances between all of the pairs you found. In the example above, this is 2 + 1 + 0 + 1 + 2 + 5, a total distance of 11!

Your actual left and right lists contain many location IDs. What is the total distance between your lists?

## --- Part Two ---

Your analysis only confirmed what everyone feared: the two lists of location IDs are indeed very different.

Or are they?

The Historians can't agree on which group made the mistakes or how to read most of the Chief's handwriting, but in the commotion you notice an interesting detail: a lot of location IDs appear in both lists! Maybe the other numbers aren't location IDs at all but rather misinterpreted handwriting.

This time, you'll need to figure out exactly how often each number from the left list appears in the right list. Calculate a total similarity score by adding up each number in the left list after multiplying it by the number of times that number appears in the right list.

Here are the same example lists again:

```text
3   4
4   3
2   5
1   3
3   9
3   3
```

For these example lists, here is the process of finding the similarity score:

- The first number in the left list is 3. It appears in the right list three times, so the similarity score increases by 3 * 3 = 9.
- The second number in the left list is 4. It appears in the right list once, so the similarity score increases by 4 * 1 = 4.
- The third number in the left list is 2. It does not appear in the right list, so the similarity score does not increase (2 * 0 = 0).
- The fourth number, 1, also does not appear in the right list.
- The fifth number, 3, appears in the right list three times; the similarity score increases by 9.
- The last number, 3, appears in the right list three times; the similarity score again increases by 9.

So, for these example lists, the similarity score at the end of this process is 31 (9 + 4 + 0 + 0 + 9 + 9).

Once again consider your left and right lists. What is their similarity score?

## Solution

### Onyx

- author: Brendan Hansen
- Original Solution: https://github.com/brendanfh/onyx-aoc-2024/blob/master/solutions/day01.onyx

In this solution we can see an example usage of the following items:

- [`~~` Operator]()
- [conv.parse/2](https://github.com/onyx-lang/onyx/blob/2612565e0dd479032c91dbe304f8909f7c79ee8a/core/conv/parse.onyx#L125)
- [Iterator.zip/2](https://github.com/onyx-lang/onyx/blob/2612565e0dd479032c91dbe304f8909f7c79ee8a/core/container/iter.onyx#L421)
- [Iterator.map/2](https://github.com/onyx-lang/onyx/blob/2612565e0dd479032c91dbe304f8909f7c79ee8a/core/container/iter.onyx#L205)
- [Iterator.sum/1](https://github.com/onyx-lang/onyx/blob/2612565e0dd479032c91dbe304f8909f7c79ee8a/core/container/iter.onyx#L890)
- [os.get_contents/1](https://github.com/onyx-lang/onyx/blob/2612565e0dd479032c91dbe304f8909f7c79ee8a/core/os/file.onyx#L111)
- [str.bisect/2](https://github.com/onyx-lang/onyx/blob/2612565e0dd479032c91dbe304f8909f7c79ee8a/core/string/string.onyx#L648)
- [str.strip_whitespace/1](https://github.com/onyx-lang/onyx/blob/2612565e0dd479032c91dbe304f8909f7c79ee8a/core/string/string.onyx#L315)
- [Slice.sort/2](https://github.com/onyx-lang/onyx/blob/2612565e0dd479032c91dbe304f8909f7c79ee8a/core/container/slice.onyx#L256)
- [Slice.count_where/2](https://github.com/onyx-lang/onyx/blob/2612565e0dd479032c91dbe304f8909f7c79ee8a/core/container/slice.onyx#L639)

<details>
<summary>Show Solution</summary>

```go
use core {*}

// MARK: Input

/// The data structure will be two lists of integers (32 bit).
Input :: struct {
    first: [..] i32;
    second: [..] i32;
}

// MARK: - Input Procedures

/// This procedure will read the input file and return a string, trimming whitespace
input_read_content :: () -> str {
    return os.get_contents("input.txt")
        |> str.strip_whitespace()
}

/// This will take the content text and transform it to a data structure that can be used in the solution.
input_to_pairs :: (content : str) -> Input {

    input := Input.{}

    for row in str.split_iter(content, "\n") {
        // Split the row in two columns using the space as separator
        first_string, second_string := str.bisect(row, "   ")

        // Convert the strings to integers, so we can do math later
        first_number := conv.parse(i32, first_string)!
        second_number := conv.parse(i32, second_string)!

        // Store the numbers in each list
        input.first->push(first_number)
        input.second->push(second_number)
    }

    return input
}

/// Sort the input lists in ASC order
input_sort :: (input: Input) -> Input {
    Slice.sort(input.first, (a, b) => a - b)
    Slice.sort(input.second, (a, b) => a - b)
    return input
}

/// Creates an Iterator so we can use enumerable procedures
input_to_iterator :: (input: Input) -> Iterator(Pair(i32, i32)) {
    return Iterator.zip(
        Iterator.from(input.first)
        Iterator.from(input.second)
    )
}

/// Read the contents and prepare it for the solutions
get_input :: () -> Input {
    return input_read_content()
    |> input_to_pairs()
}

/// Count the similarity score comparing first column with a number in second column
input_count_total_similarity :: (input: Input) -> u64 {
    total: u64 = 0
    for first_number in input.first {
        // We use the ~~ operator to automatically cast the result to u64
        total += ~~(first_number * Slice.count_where(input.second, [second_number](second_number == first_number)))
    }

    return total
}

// MARK: Solutions
part1 :: () {
    get_input()
    |> input_sort()
    |> input_to_iterator()
    |> Iterator.map(element => math.abs(element.first - element.second))
    |> Iterator.sum()
    |> printf("The part 1 solution is: {}\n", _)
}

part2 :: () {
    get_input()
    |> input_count_total_similarity()
    |> printf("The part 2 solution is: {}\n", _)
}

// MARK: main
main :: () {
    part1()
    part2()
}
```

</details>

## Elixir

- author: TORIFUKUKaiou
- Original Solution: https://qiita.com/torifukukaiou/items/2b02ac5054b0bfe5817f

<details>
<summary>Show Solution</summary>

```elixir
defmodule AdventOfCode2024Day1Part1 do
  def calculate_total_distance(left_list, right_list) do
    sorted_left = Enum.sort(left_list)
    sorted_right = Enum.sort(right_list)

    distances =
      Enum.zip(sorted_left, sorted_right)
      |> Enum.map(fn {left, right} -> abs(left - right) end)

    Enum.sum(distances)
  end

  def parse_input(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      [left, right] = String.split(line, ~r/\s+/, trim: true)
      {String.to_integer(left), String.to_integer(right)}
    end)
    |> Enum.unzip()
  end
end

# テストデータ
input = """
3   4
4   3
2   5
1   3
3   9
3   3
"""

{left_list, right_list} = AdventOfCode2024Day1Part1.parse_input(input)

# 距離の計算
total_distance = AdventOfCode2024Day1Part1.calculate_total_distance(left_list, right_list)
IO.puts("Total distance: #{total_distance}")
```

```elixir
defmodule AdventOfCode2024Day1Part2 do
  def calculate_similarity_score(left_list, right_list) do
    right_counts = Enum.frequencies(right_list)

    Enum.reduce(left_list, 0, fn num, acc ->
      count = Map.get(right_counts, num, 0)
      acc + num * count
    end)
  end

  def parse_input(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      [left, right] = String.split(line, ~r/\s+/, trim: true)
      {String.to_integer(left), String.to_integer(right)}
    end)
    |> Enum.unzip()
  end
end

# テストデータ
input = """
3   4
4   3
2   5
1   3
3   9
3   3
"""

{left_list, right_list} = AdventOfCode2024Day1Part2.parse_input(input)

# 類似度スコアの計算
similarity_score = AdventOfCode2024Day1Part2.calculate_similarity_score(left_list, right_list)
IO.puts("Similarity score: #{similarity_score}")
```

</details>
