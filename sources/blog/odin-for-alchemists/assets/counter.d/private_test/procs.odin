package Main

import "core:fmt"
import pkg "./package1"
import "core:strings"

main :: proc() {
  fmt.println(pkg.public_proc())
  //fmt.println(pkg.private_proc()) // should crash

  hello := proc(name: string, shows: int) {
    fmt.printfln("%s: %d", name, shows)
  }

  hello(name = "Camilo", shows = 2)

  fmt.println(concatenate({"Hello", "Wolrld"}))
}
