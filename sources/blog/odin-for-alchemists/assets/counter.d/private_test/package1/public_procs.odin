package Package1

public_proc :: proc() -> string {
  if private_proc() == 42 {
    return "Is 42"
  }

  return "Nope"
}
