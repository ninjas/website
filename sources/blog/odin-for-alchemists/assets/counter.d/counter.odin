// odin run counter.odin -file
package Counter

import "core:fmt"
import "core:strings"


count_numbers :: proc(items: []int) -> string {
  return fmt.tprintf("%d numbers", len(items))
}

count_words :: proc(items: []string) -> string {
  return fmt.tprintf("%d words", len(items))
}

count :: proc {
  count_words,
  count_numbers
}

subtract :: proc(a: int, b: int) -> int {
  return a - b
}

main :: proc() {
  numbers := []int{1, 2, 3}
  fmt.println(count(numbers))

  words := []string{"one", "two", "three"}
  fmt.println(count(words))

  _ = fmt.tprintf("Hello this is 4 + 2 = %d", 4 + 2)


  fmt.println(strings.concatenate([]string{"My String", " is good"}))

  fmt.println(strings.concatenate("My String", " is good"))

  add := proc(a: int, b: int) -> int {
    return a + b
  }

  fmt.println(add(1, 2))

  subtract_pointer := subtract

  fmt.println(subtract_pointer(6,3))

  fmt.println([]any{"1", 2})

  fmt.println([?]string{"1", "2"})


  fmt.println(map[string]any {
    "this" = "is my map",
    "answer" = 42,
  })

}
