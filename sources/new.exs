#!/usr/bin/env elixir

Mix.install([
  {:jason, "~> 1.4"}
])

require Logger

defmodule Tools do
  # from https://mudssrali.com/blog/slugify-a-string-in-elixir
  def slugify(text) when is_binary(text) do
    text
    |> String.downcase()
    |> String.trim()
    |> String.normalize(:nfd)
    |> String.replace(~r/[^a-z0-9\s-]/u, "  ")
    |> String.replace(~r/[\s-]+/, "-", global: true)
  end

  def slugify(_), do: ""
end

title =
  System.argv()
  |> Enum.at(0)
  |> String.trim()

if is_nil(title) or title == "" do
  raise "Title is required.\n$ make new title=\"Example Title\""
end

slug = Tools.slugify(title)

now =
  DateTime.utc_now()
  |> DateTime.to_iso8601()

frontmatter =
  %{
    title: title,
    slug: slug,
    published_at: now,
    published: false,
    description: "",
    image: "https://ninjas.cl/blog/#{slug}/assets/cover.jpg",
    tags: [],
    author: "Camilo Castro <camilo@ninjas.cl>",
    author_url: "https://ninjas.cl"
  }
  |> Jason.encode!()
  |> Jason.Formatter.pretty_print()

content = """
---
#{frontmatter}
---

![](assets/cover.jpg)
"""

post = Path.join(["sources", "blog", slug])
assets = Path.join([post, "assets"])

File.mkdir_p!(assets)

file = Path.join([post, "#{slug}.md.eex"])

File.write!(file, content, [:write])

Logger.info("Create new blog post #{slug}")
