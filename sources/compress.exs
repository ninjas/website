#!/usr/bin/env elixir

require Logger

defmodule Compress do
  defp md5(string) do
    :crypto.hash(:md5, string)
    |> Base.encode16(case: :lower)
  end

  defp is_in_cache?(file) do
    cache = Path.join([".optimized", md5(file)])
    File.exists?(cache)
  end

  defp save_to_cache(file) do
    cache = Path.join([".optimized", md5(file)])
    File.cp!(file, cache)
  end

  defp use_cache(file) do
    Logger.debug("Using Optimized Cache #{file}")
    cache = Path.join([".optimized", md5(file)])
    File.cp!(cache, file)
  end

  defp images(files, :jpg) do
    Enum.map(files, fn path ->
      Task.async(fn ->
        if is_in_cache?(path) do
          use_cache(path)
        else
          Logger.debug("Optimizing #{path}")

          System.cmd(
            "mogrify",
            [
              "-strip",
              "-interlace",
              "Plane",
              "-sampling-factor",
              "4:2:0",
              "-quality",
              "55%",
              Path.expand(path)
            ],
            into: IO.stream()
          )

          save_to_cache(path)
        end
      end)
    end)
  end

  defp images(files, :png) do
    Enum.map(files, fn path ->
      Task.async(fn ->
        if is_in_cache?(path) do
          use_cache(path)
        else
          Logger.debug("Optimizing #{path}")

          System.cmd(
            "mogrify",
            [
              "-filter",
              "Triangle",
              "-define",
              "filter:support=2",
              "-unsharp",
              "0.25x0.08+8.3+0.045",
              "-dither",
              "None",
              "-posterize",
              "136",
              "-quality",
              "82",
              "-define",
              "png:compression-level=9",
              "-define",
              "png:compression-strategy=1",
              "-define",
              "png:exclude-chunk=all",
              "-interlace",
              "none",
              Path.expand(path)
            ],
            into: IO.stream()
          )

          save_to_cache(path)
        end
      end)
    end)
  end

  def all(files, type) do
    Task.await_many(images(files, type), :infinity)
  end
end

File.mkdir_p!(".optimized")

jpg = Path.wildcard("docs/blog/**/**/*.jp*g")
png = Path.wildcard("docs/blog/**/**/*.png")

Compress.all(jpg, :jpg)
Compress.all(png, :png)

Logger.info("Image Optimization Complete")
