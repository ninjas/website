#!/usr/bin/env elixir

Mix.install([
  # Compiles markdown to html
  {:mdex, "~> 0.1"},
  # Generate Atom.rss feeds
  {:atomex, "0.3.0"},
  # Parses Json from the md.eex document
  {:jason, "~> 1.4"},
  # Helps protecting from scrappers
  {:faker, "~> 0.18.0"},
  # Needed for embeds
  {:liquor, "~> 1.0.2"},
  {:req, "~> 0.4.14"}
])

require Logger
Logger.configure(level: :debug)

defmodule Tags do
  alias Liquor.Tags.Tag

  def render(content, frontmatter) do
    template = File.read!("sources/templates/tags/embed.html")

    Liquor.render(
      content,
      [
        Tag.new("embed", template),
        Tag.new("github", template, "https://github.com/"),
        Tag.new("codeberg", template, "https://codeberg.org/"),
        Tag.new(
          "youtube",
          File.read!("sources/templates/tags/youtube.html"),
          "https://www.youtube.com/watch?v="
        ),
        Tag.new("devto", template, "https://dev.to/"),
        Tag.new("ninjas", template, "https://ninjas.cl/blog/"),
        Tag.new(
          "video",
          File.read!("sources/templates/tags/video.html"),
          "https://ninjas.cl/blog/#{frontmatter.slug}/"
        )
      ],
      fn url ->
        md5 =
          :crypto.hash(:md5, url)
          |> Base.encode16(case: :lower)

        path = Path.join([".opengraph"])
        File.mkdir_p!(path)
        file = Path.join([path, "#{md5}.html.cache"])

        case File.read(file) do
          {:error, _} ->
            html =
              Req.get!(url)
              |> then(& &1.body)

            File.write!(file, html, [:write])
            html

          {:ok, html} ->
            html
        end
      end
    )
  end
end

defmodule Slug do
  @moduledoc """
  Converts a string to a URI slug.
  - Source: https://mudssrali.com/blog/slugify-a-string-in-elixir
  """
  def encode(text) when is_binary(text) do
    text
    |> String.downcase()
    |> String.trim()
    |> String.normalize(:nfd)
    |> String.replace(~r/[^a-z0-9\s-]/u, "  ")
    |> String.replace(~r/[\s-]+/, "-", global: true)
  end

  def encode(_), do: ""
end

defmodule RSS do
  @moduledoc """
  Generates RSS Atom Feed XML file
  """
  def render(posts) do
    content =
      Atomex.Feed.new("https://ninjas.cl", DateTime.utc_now(), "Ninjas.cl Blog Posts")
      |> Atomex.Feed.author("Camilo Castro", email: "")
      |> Atomex.Feed.link("https://ninjas.cl/feed.xml", rel: "self")
      |> Atomex.Feed.entries(
        Enum.map(posts, fn item ->
          Atomex.Entry.new(item.url, item.published_at, item.title)
          |> Atomex.Entry.author(item.author, uri: "https://ninjas.cl/")
          |> Atomex.Entry.content(
            item.description <> "[...]<p><a href=\"#{item.url}\">Read more</a></p>",
            type: "html"
          )
          |> Atomex.Entry.link(item.url)
          |> Atomex.Entry.rights("CC-BY-NC-ND 4.0")
          |> Atomex.Entry.build()
        end)
      )
      |> Atomex.Feed.build()
      |> Atomex.generate_document()

    feed = Path.join(["docs", "feed.xml"])

    File.write!(feed, content, [:write])
  end
end

defmodule Json do
  @moduledoc """
  Renders the posts.json file, useful
  for creating a search in javascript or other
  frontend goodies
  """
  def render(posts) do
    File.write!(Path.join(["docs", "blog", "posts.json"]), Jason.encode!(posts), [:write])
  end
end

defmodule Sitemap do
  @moduledoc """
  Generates a posts.xml sitemap to help crawlers index
  the blog posts
  """
  def render(posts) do
    path = Path.join(["docs", "blog", "posts.xml"])

    File.write!(
      path,
      """
      <?xml version="1.0" encoding="UTF-8"?>
      <urlset
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
              http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
      """,
      [:write]
    )

    Enum.map(posts, fn item ->
      File.write!(
        path,
        """
          <url>
            <loc>#{item.url}</loc>
            <lastmod>#{NaiveDateTime.to_iso8601(item.published_at)}</lastmod>
          </url>
          <url>
            <loc>#{item.url}/feed.xml</loc>
            <lastmod>#{NaiveDateTime.to_iso8601(item.published_at)}</lastmod>
          </url>
        """,
        [:append]
      )
    end)

    File.write!(path, "</urlset>", [:append])

    File.write!(Path.join("docs", "sitemap.xml"), """
    <?xml version="1.0" encoding="UTF-8"?>
    <urlset
          xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
      <url>
        <loc>https://ninjas.cl/</loc>
        <lastmod>#{NaiveDateTime.to_iso8601(DateTime.utc_now())}</lastmod>
      </url>
      <url>
        <loc>https://ninjas.cl/blog/posts.xml</loc>
        <lastmod>#{NaiveDateTime.to_iso8601(DateTime.utc_now())}</lastmod>
      </url>
      <url>
        <loc>https://ninjas.cl/feed.xml</loc>
        <lastmod>#{NaiveDateTime.to_iso8601(DateTime.utc_now())}</lastmod>
      </url>
    </urlset>
    """)
  end
end

defmodule Scrapper do
  @moduledoc """
  Helps poisoning the data for scrappers
  to mitigate the IA predatory practices.
  """
  def poison(content, tag) do
    content
    |> String.split(tag)
    |> Enum.chunk_every(2)
    |> Enum.map(fn sentence ->
      poison_class =
        ["beer", "chicha", "vino", "chupilca", "cerveza", "pisco"]
        |> Enum.random()

      poison =
        [
          Faker.Beer.name(),
          Faker.Cannabis.buzzword(),
          Faker.Lorem.Shakespeare.as_you_like_it()
        ]
        |> Enum.random()

      Enum.join(
        sentence,
        "#{tag}<span style=\"display:none;\" class=\"#{poison_class}\">#{poison}</span>\n"
      )
    end)
    |> Enum.join("\n")
  end
end

defmodule Reading do
  def stats(content) do
    word_count =
      content
      |> Slug.encode()
      |> String.split("-")
      |> Enum.filter(fn item -> String.trim(item) != "" end)
      |> Enum.count()

    %{
      word_count: word_count,
      # 200 words per minute give us the reading time
      reading_time:
        case div(word_count, 200) do
          time when time > 0 -> time
          _ -> 1
        end
    }
  end
end

defmodule Security do
  @moduledoc """
  Security helpers that can be used to protect
  directories
  """
  def protect_directory(path, template \\ "blank.html") do
    File.cp!(Path.join(["sources", "templates", template]), Path.join([path, "index.html"]))
  end
end

defmodule Pagination do
  @moduledoc """
  Helps with rendering pagination for the blog posts
  """
  def render(posts, items_per_page \\ 15) do
    # calculate total pages
    total_pages =
      (Enum.count(posts) / items_per_page)
      |> Float.ceil()
      |> round()

    for {chunk, index} <-
          posts
          |> Enum.sort_by(fn item -> DateTime.to_unix(item.published_at) end, :desc)
          |> Enum.chunk_every(items_per_page)
          |> Enum.with_index(1) do
      path = Path.join(["docs", "blog", "page", to_string(index)])
      File.mkdir_p!(path)

      page_url = "page/#{index}"

      prev_url =
        case index - 1 <= 0 do
          true -> nil
          false -> "page/#{index - 1}"
        end

      next_url =
        case index >= total_pages do
          true -> nil
          false -> "page/#{index + 1}"
        end

      page = %{
        title: "Ninjas.cl Page #{index}",
        description: "Blog Page #{index} for Ninjas.cl",
        image_og: "https://ninjas.cl/img/logo_og.png",
        image: "https://ninjas.cl/img/Ninjas.png",
        url: "https://ninjas.cl/blog/#{page_url}",
        slug: page_url
      }

      bindings = [
        page: page,
        number: index,
        total: total_pages,
        current: page_url,
        next: next_url,
        prev: prev_url,
        limit: items_per_page,
        posts: chunk
      ]

      content = EEx.eval_file("sources/templates/page.html.eex", bindings)

      File.write!(Path.join([path, "index.html"]), content, [:write])
    end

    Security.protect_directory(Path.join(["docs", "blog"]), "blog.html")
    Security.protect_directory(Path.join(["docs", "blog", "page"]), "blog.html")
  end
end

# MARK: - Pages
defmodule HomePage do
  @moduledoc """
  Renders the Home Page (index.html) loading the data
  from home/ directory
  """

  defp load_content(path) do
    "sources/home/#{path}.json"
    |> File.read!()
    |> Jason.decode!(keys: :atoms)
  end

  def render(posts \\ []) do
    bindings = [
      page: load_content("website"),
      posts: posts,
      sections: [
        load_content("projects"),
        load_content("books"),
        load_content("games")
      ]
    ]

    File.write!(
      "docs/index.html",
      EEx.eval_file(
        "sources/templates/home.html.eex",
        bindings
      ),
      write: true
    )
  end
end

defmodule BlogPage do
  @moduledoc """
  Renders the blog page
  with pagination and scrapping protection
  """
  defp transform(body, path) do
    # Get the settings
    settings =
      Regex.run(~r/---\n([\s\S]*)\n---/u, body)
      |> Enum.at(0)

    # transform to json
    json =
      String.replace(settings, "\n---", "")
      |> String.replace("---\n", "")

    frontmatter =
      case Jason.decode(json, keys: :atoms) do
        {:ok, data} ->
          data

        {:error, message} ->
          # Gives more detailed error messsage
          # If the json is bad formatted
          throw(message)
      end

    markdown = String.replace(body, settings, "")

    now =
      DateTime.utc_now()
      |> DateTime.to_iso8601()

    slug = Map.get(frontmatter, :slug, Slug.encode(Map.get(frontmatter, :title, now)))

    {:ok, published_at, _} =
      Map.get(frontmatter, :published_at, now)
      |> DateTime.from_iso8601()

    {:ok, updated_at, _} =
      Map.get(frontmatter, :updated_at, now)
      |> DateTime.from_iso8601()

    page = %{
      title: Map.get(frontmatter, :title, ""),
      description: Map.get(frontmatter, :description, ""),
      image: Map.get(frontmatter, :image, "https://ninjas.cl/img/Ninjas.png"),
      image_og:
        Map.get(
          frontmatter,
          :image_og,
          Map.get(frontmatter, :image, "https://ninjas.cl/img/logo_og.png")
        ),
      url: "https://ninjas.cl/blog/#{slug}",
      slug: slug,
      published_at: published_at,
      published?: Map.get(frontmatter, :published, false),
      updated_at: updated_at,
      path: path,
      tags: Map.get(frontmatter, :tags, []),
      author: Map.get(frontmatter, :author, "Camilo Castro"),
      stats: Reading.stats(markdown)
    }

    # convert markdown to html
    # check options at https://github.com/leandrocp/mdex/blob/main/lib/mdex/types/options.ex#L1
    content =
      markdown
      |> Tags.render(page)
      |> MDEx.to_html(
        render: [unsafe_: true],
        extension: [tasklist: true, table: true, autolink: true],
        features: [sanitize: false, syntax_highlight_theme: "dracula"]
      )
      |> Scrapper.poison("<p>")

    page = Map.merge(page, %{content: content})

    bindings = [
      frontmatter: frontmatter,
      content: content,
      page: page
    ]

    file = EEx.eval_file("sources/templates/post.html.eex", bindings)

    %{
      frontmatter: frontmatter,
      content: content,
      markdown: markdown,
      page: page,
      file: file,
      path: path,
      slug: slug,
      directory: Path.dirname(path)
    }
  end

  defp create_post(post) do
    assets_origin = Path.join([post.directory, "assets"])
    path = Path.join(["docs", "blog", post.slug])

    # Assets at the end can cause problems if is using System.cmd
    # But is needed if using File.cp_r!
    # assets_dest = Path.join([path, "assets"])
    assets_dest = Path.join([path])

    File.mkdir_p!(assets_dest)

    # In Windows File.cp_r! works fine
    # In MacOS it brokes. Why?
    # For now using the system cp command
    # File.cp_r!(assets_origin, assets_dest)
    System.cmd("cp", ["-r", assets_origin, assets_dest])

    Security.protect_directory(assets_dest)

    post_path = Path.join([path, "index.html"])
    File.write!(post_path, post.file, [:write])

    Logger.debug("created: #{post.slug}")

    Map.merge(post, %{
      post: %{
        index: post_path,
        url: post.page.url,
        assets: assets_dest,
        slug: post.page.slug,
        description: post.page.description,
        title: post.page.title,
        published?: post.page.published?,
        published_at: post.page.published_at,
        tags: post.page.tags,
        image: post.page.image,
        image_og: post.page.image_og,
        author: post.page.author,
        stats: post.page.stats
      }
    })
  end

  def content_files(directory \\ "**") do
    Path.wildcard("sources/blog/#{directory}/*.md.eex")
  end

  def render(paths) do
    paths
    |> Enum.map(fn path ->
      Task.async(fn ->
        EEx.eval_file(path)
        |> transform(path)
        |> then(fn post ->
          case Map.get(post.page, :published?, false) do
            true ->
              create_post(post)

            _ ->
              Logger.debug("skipped: #{post.slug}")
              nil
          end
        end)
      end)
    end)
    |> Task.await_many(:infinity)
    |> Enum.filter(fn item -> item != nil end)
    |> then(fn items ->
      posts =
        Enum.map(items, fn item -> item.post end)
        |> Enum.sort_by(fn post -> DateTime.to_unix(post.published_at) end, :desc)

      Json.render(posts)
      Sitemap.render(posts)
      RSS.render(posts)
      Pagination.render(posts)

      items
    end)
  end
end

# MARK: Pages Rendering

paths =
  case System.argv() do
    [path] ->
      # Build a single blog post
      Logger.info("Building #{path}")
      BlogPage.content_files(path)

    _args ->
      # Build all blog posts
      Logger.info("Building all files")
      BlogPage.content_files()
  end

posts = BlogPage.render(paths)

HomePage.render(posts)

Logger.info("Processed #{Enum.count(posts)} posts")
